using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SkinShopItem : MonoBehaviour
{
    [SerializeField] private SkinManager skinManager;
    [SerializeField] private int skinIndex;
    [SerializeField] private Button buyButton;
    [SerializeField] private TMP_Text costText;
    private Animator anim;
    private Skin skin;

     void Start()
    {
        skin = skinManager.skins[skinIndex];
        anim = GetComponent<Animator>();


        GetComponent<Image>().sprite = skin.sprite;

        if (skinManager.isUnlocked(skinIndex))
        {
            buyButton.gameObject.SetActive(false);
        }
        else
        {
            buyButton.gameObject.SetActive(true);
            costText.text = skin.cost.ToString();
        }
    }
    public void OnSkinPressed()
    {
        if (skinManager.isUnlocked(skinIndex))
        {
            skinManager.SelectSkin(skinIndex);
        }
    }
    public void OnBuyButtonPressed()
    {
        int cherry = PlayerPrefs.GetInt("Cherry", 0);

        //Unlock the skin
        if(cherry >= skin.cost && !skinManager.isUnlocked(skinIndex))
        {
            PlayerPrefs.SetInt("Cherry", cherry - skin.cost);
            skinManager.Unlock(skinIndex);
            buyButton.gameObject.SetActive(false);
            skinManager.SelectSkin(skinIndex);
        }
        else
        {
            Debug.Log("Not enough cherries! "); 
        }
    }
}
