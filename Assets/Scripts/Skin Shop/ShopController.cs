using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class ShopController : MonoBehaviour
{
    [SerializeField] private Image selectedSkin;
    [SerializeField] private TMP_Text cherryText;
    [SerializeField] private SkinManager skinManager;

    void Update()
    {
        //cherryText.text = "Cherry: " + PlayerPrefs.GetInt("Cherry");
        selectedSkin.sprite = skinManager.GetSelectedSkin().sprite;
        
    }

    public void LoadMenu() => SceneManager.LoadScene("Start Screen");
}
