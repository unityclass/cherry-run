using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameMenu : MonoBehaviour
{
    [SerializeField] private GameObject hudMenu;
    [SerializeField] private PauseMenu pauseMenu;

    public void BackToMain()
    {
        SceneManager.LoadScene("Start Screen");
        GameManager.Instance.ResumeGame();
    }
    public void SelectLevel()
    {
        SceneManager.LoadScene("Level Selector");
        GameManager.Instance.ResumeGame();
    }

    public void SelectCharacters()
    {
        SceneManager.LoadScene("Shop");
        GameManager.Instance.ResumeGame();
    }

    public void PauseGame()
    {
        hudMenu.SetActive(false);
        pauseMenu.Open();
        GameManager.Instance.PauseGame();
    }
    public void ResumeGame()
    {
        hudMenu.SetActive(true);
        pauseMenu.Close();
        GameManager.Instance.ResumeGame();
    }
}
