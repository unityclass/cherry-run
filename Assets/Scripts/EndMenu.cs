using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndMenu : MonoBehaviour
{
    public void BackToMain()
    {
        SceneManager.LoadScene("Start Screen");
    }
    public void Quit()
    {
        Application.Quit();
    }
}
