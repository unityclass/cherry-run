using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelector : MonoBehaviour
{ 
    public void SelectLevel1()
    {
        SceneManager.LoadScene("Level 1");
    }
    public void SelectLevel2()
    {
        SceneManager.LoadScene("Level 2");
    }
    public void SelectLevel3()
    {
        SceneManager.LoadScene("Level 3");
    }
}
